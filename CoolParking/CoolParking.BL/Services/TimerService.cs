﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Timers;
using System;

namespace CoolParking.BL.Services
{
    class TimerService : ITimerService
    {
        public double Interval { get; set; }
        private static System.Timers.Timer PTimer;
        public event ElapsedEventHandler Elapsed;

        public void Start()
        {
            PTimer = new System.Timers.Timer(this.Interval); // Settings.paymentWriteOffPeriod*1000
            PTimer.Elapsed += Elapsed;
            PTimer.AutoReset = true;
            PTimer.Enabled = true;
        }
        public void Stop()
        {
            PTimer.Stop();
        }
        public void Dispose()
        {
            PTimer.Dispose();
        }
    }
}