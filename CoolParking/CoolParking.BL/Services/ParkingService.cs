﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    class ParkingService : Parking, IParkingService
    {
        decimal Balance = Settings.parkingInitialBalance;
        int Capacity = Settings.parkingCapacity;
        
        private static ParkingService instance;
        private ParkingService() { }

        public static ParkingService getInstance()
        {
            if (instance == null)
                instance = new ParkingService();
            return instance;
        }

        public decimal GetBalance()
        {
            return this.Balance;
        }

        public int GetCapacity()
        {
            return this.Capacity;
        }
        public int GetFreePlaces()
        {
            return this.Capacity - this.Vehicles.Count;
        }
        ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(this.Vehicles);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            Vehicles.Add(vehicle);
        }

        private IEnumerable<Vehicle> GetVehicleById(string vehicleId)
        {
            IEnumerable<Vehicle> vehiclesId = from Vehicle in Vehicles
                                              where Vehicle.GetVehicleId() == vehicleId
                                              select Vehicle;
            return vehiclesId;
        }
        public void RemoveVehicle(string vehicleId)
        {
            foreach (Vehicle Vehicle in GetVehicleById(vehicleId))
            {
                this.Vehicles.Remove(Vehicle);
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            foreach (Vehicle Vehicle in GetVehicleById(vehicleId))
            {
                Vehicle.IncreaseVehicleBalance(sum);
            }
        }
        TransactionInfo[] GetLastParkingTransactions();
        string ReadFromLog();

    }
}