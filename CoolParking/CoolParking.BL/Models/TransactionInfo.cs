﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
namespace CoolParking.BL.Models
{
    class TransactionInfo
    {
        TimeSpan transactionTime = new TimeSpan();
        string vehicleId;
        decimal transactionSum;
        public TransactionInfo(TimeSpan currentTime, string vehicleId, decimal transactionSum)
        {
            this.transactionTime = currentTime;
            this.vehicleId = vehicleId;
            this.transactionSum = transactionSum;
        }
    }
}