﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal parkingInitialBalance = 0;
        public static int parkingCapacity = 10;

        public static int paymentWriteOffPeriod = 5;
        public static int paymentWritingToLogPeriod = 60;

        public static double tariffPassengerCar = 2;
        public static double tariffTruck = 5;
        public static double tariffBus = 3.5;
        public static double tariffMotorcycle = 1;

        public static double fineRate = 2.5;
    }
}