﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string Id = "AA-0000-AA";
        private VehicleType VehicleType;
        internal decimal Balance;

        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            this.Id = Id;
            this.VehicleType = VehicleType;
            this.Balance = Balance;
        }
        static string GenerateRandomRegistrationPlateNumber()
        {
            Random rand = new Random();

            char[] identifier = new char[10];
            for (int symbolCnt = 0; symbolCnt < 10; symbolCnt++)
            {
                switch (symbolCnt)
                {
                    case < 2:
                        identifier[symbolCnt] = Convert.ToChar(rand.Next(65, 91));
                        break;
                    case < 3:
                        identifier[symbolCnt] = '-';
                        break;
                    case < 7:
                        identifier[symbolCnt] = Convert.ToChar(rand.Next(48, 58));
                        break;
                    case < 8:
                        identifier[symbolCnt] = '-';
                        break;
                    case < 10:
                        identifier[symbolCnt] = Convert.ToChar(rand.Next(65, 91));
                        break;
                    default:
                        break;
                }
            }
            return String.Join("", identifier);
        }
        public string GetVehicleId()
        {
            return this.Id;
        }

        public void IncreaseVehicleBalance(decimal sum)
        {
            this.Balance += sum;
        }
    }
}